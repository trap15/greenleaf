/*****************************************************************************
 *  SquidMach Bootstrap v0.1                                                 *
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *                                 U S I N G                                 *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  µChecksum 0.2                                                            *
 *  Checksum alghoritm for data verification.                                *
 *  (C) 2009 - The Lemon Man                                                 *
 *  (C) 2009 - SquidMan                                                      *
 *****************************************************************************/
	.set	noreorder
	.set	nomove

	.include "syscall.inc"
	
	.equ	BOOTSUPPORT_ENTRY,			0x10000000
	.equ	BOOTSUPPORT_EXCEPTION,			0x10000004

	.equ	HW_REG_HWVERSION,			0x40000000
	.equ	HW_REG_PRINT_TEXT,			0x40000004
	.equ	HW_REG_PRINT_HEX,			0x40000008
	.equ	HW_REG_PRINT_INT,			0x40000010
	.equ	HW_REG_POWER,				0x40000018
	.equ	HW_REG_READKEYS_1,			0x40000020
	.equ	HW_REG_READKEYS_2,			0x40000028
	/* We do this because we offset from here when reading, instead of using each seperately */
	.equ	HW_REG_READKEYS,			HW_REG_READKEYS_1
	.equ	HW_REG_RANDOM,				0x40000030
	.equ	HW_REG_SHUFFLE,				0x40000038
	
	.equ	HW_POWERON_GRAPHICS,			0x00000001
	.equ	HW_POWERON_INPUT,			0x00000002
	
	.equ	BOOTSTRAP_MAJOR,			0
	.equ	BOOTSTRAP_MINOR,			1
	.equ	BOOTSTRAP_REV,				1
	
	.equ	VALID_CHECKSUM,				0
	.equ	CHECK_MEMORY,				1
	.equ	CHECK_XSUM,				1


.section .info, "ax", @progbits
about_1:
	.asciiz "SquidMach Bootstrap v"
about_2:
	.asciiz "\nCopyright (C)2009 SquidMan (Alex Marshall)\n"
about_3:
	.asciiz "Protected under the GNU GPLv2\n\n"
period:
	.asciiz "."
space:
	.asciiz " "
dash:
	.asciiz " - "
newline:
	.asciiz "\n"
testing_1:
	.asciiz "Testing hardware...\n"
testing_2:
	.asciiz "Checking video systems... "
testing_3:
	.asciiz "Enabling input systems... "
testing_4:
	.asciiz "Verifying memory...\n"
testing_memory:
	.asciiz "Verifying memory section "
testing_5:
	.asciiz "Verifying BIOS code... "
testing_6:
	.asciiz "Shuffling random number generator... "
testing_done:
	.asciiz "Hardware test successful!\n"
success:
	.asciiz "SUCCESS!\n"
failure:
	.asciiz "FAILURE!\n"
wat:
	.asciiz "End bootstrap. Launching executable...\n\n"
valid_xsum:
	.dword	VALID_CHECKSUM

.section .code, "ax", @progbits
	.org 0x0000			/* Will be at 0xBFC00000 */
					/* Standard SquidMach boot procedure */
_start:
	/* Set the BEV bit. */
	mfc0	$t0, $12
	or	$t0, 0x400000
	mtc0	$t0, $12
	
	jal	init_mach
	
	move	$v0, $zero
	move	$v1, $zero
	move	$a0, $zero
	move	$a1, $zero
	move	$a2, $zero
	move	$a3, $zero
	move	$t0, $zero
	move	$t1, $zero
	move	$t2, $zero
	move	$t3, $zero
	move	$t4, $zero
	move	$t5, $zero
	move	$t6, $zero
	move	$t7, $zero
	move	$t8, $zero
	move	$t9, $zero
	move	$s0, $zero
	move	$s1, $zero
	move	$s2, $zero
	move	$s3, $zero
	move	$s4, $zero
	move	$s5, $zero
	move	$s6, $zero
	move	$s7, $zero
	move	$k0, $zero
	move	$k1, $zero
	
	la	$a0, wat
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	li	$a0, BOOTSTRAP_MAJOR
	li	$a1, BOOTSTRAP_MINOR
	li	$a2, BOOTSTRAP_REV
	lw	$a3, HW_REG_HWVERSION($zero)
	lw	$ra, BOOTSUPPORT_ENTRY($zero)	/* Load up the entry point and jump to it. */
	jr	$ra
	nop
hang:
	b	hang
	nop
failed:
	la	$a0, failure
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	b	hang
	nop
							/* Standard SquidMach exception handlers */
	.org 0x0200					/* Will be at 0xBFC00200 */
__exception_tlb_handle:
	mfc0	$k0, $13
	srl	$k0, $k0, 2
	lw	$k0, BOOTSUPPORT_EXCEPTION($zero)	/* Load up the exception handler and jump to it. */
	jalr	$k0
	nop
	eret

	.org 0x0280					/* Will be at 0xBFC00280 */
__exception_xtlb_handle:
	mfc0	$k0, $13
	srl	$k0, $k0, 2
	lw	$k0, BOOTSUPPORT_EXCEPTION($zero)	/* Load up the exception handler and jump to it. */
	jalr	$k0
	nop
	eret

	.org 0x0300					/* Will be at 0xBFC00300 */
__exception_cache_handle:
	mfc0	$k0, $13
	srl	$k0, $k0, 2
	lw	$k0, BOOTSUPPORT_EXCEPTION($zero)	/* Load up the exception handler and jump to it. */
	jalr	$k0
	nop
	eret

	.org 0x0380					/* Will be at 0xBFC00380 */
__exception_other_handle:
	li	$k1, 0x8
	mfc0	$k0, $13
	srl	$k0, $k0, 2
	beq	$k0, $k1, syscall_handler
	nop
	lw	$k0, BOOTSUPPORT_EXCEPTION($zero)	/* Load up the exception handler and jump to it. */
	jalr	$k0
	nop
	eret

	.org 0x0400
print_text:
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	li	$a0, 1
	eret

print_hex:
	sd	$a0, HW_REG_PRINT_HEX($zero)
	li	$a0, 1
	eret

print_int:
	sd	$a0, HW_REG_PRINT_INT($zero)
	li	$a0, 1
	eret

get_hardware_version:
	lw	$a0, HW_REG_HWVERSION($zero)
	eret

get_bootstrap_version:
	li	$a0, BOOTSTRAP_MAJOR
	li	$a1, BOOTSTRAP_MINOR
	li	$a2, BOOTSTRAP_REV
	eret

get_keys_bits:
	sll	$a0, $a0, 3
	ld	$a0, HW_REG_READKEYS($a0)	/* a0 is the keys in a bitfield. */
	eret

get_key_down:
	andi	$k0, $a0, 0x40
	andi	$a0, $a0, 0xBF
	srl	$k0, $k0, 3
	ld	$k0, HW_REG_READKEYS($k0)		/* k0 is the keys in a bitfield. */
	srlv	$k0, $k0, $a0
	andi	$a0, $k0, 1
	bnez	$a0, kd_e
	nop
	li	$a0, 0
kd_e:
	eret

get_key_up:
	andi	$k0, $a0, 0x40
	andi	$a0, $a0, 0xBF
	srl	$k0, $k0, 3
	ld	$k0, HW_REG_READKEYS($k0)		/* k0 is the keys in a bitfield. */
	srlv	$k0, $k0, $a0
	andi	$a0, $k0, 1
	li	$k0, 1
	sub	$a0, $k0, $a0
	beqz	$a0, ku_e
	nop
	li	$a0, 1
ku_e:
	eret

make_xsum:
	sd	$t0, 0($sp)
	addiu	$sp, $sp, 8
	sd	$t1, 0($sp)
	addiu	$sp, $sp, 8
	sd	$t2, 0($sp)
	addiu	$sp, $sp, 8
	sd	$t3, 0($sp)
	addiu	$sp, $sp, 8
	sd	$t4, 0($sp)
	addiu	$sp, $sp, 8
	sd	$t5, 0($sp)
	addiu	$sp, $sp, 8
	sd	$t6, 0($sp)
	addiu	$sp, $sp, 8
	
	move	$t0, $a0
	li	$t2, 2
	subu	$t1, $a1, $t2
	addu	$t1, $t1, $t0
	move	$a0, $zero
xsum_loop:
	lhu	$t2, 0($t0)
	lbu	$t3, 2($t0)
	lbu	$t4, 3($t0)
	andi	$t2, $t2, 0xB4E1
	andi	$t3, $t3, 0x87
	and	$t5, $t4, $t3
	sll	$t6, $t4, 8
	or	$t3, $t6, $t3
	xor	$t2, $t2, $t3
	andi	$t4, $t4, 0x3F
	dsllv	$t2, $t2, $t4
	daddu	$a0, $a0, $t2
	addiu	$t0, $t0, 2
	bne	$t0, $t1, xsum_loop
	nop

	li	$t0, 8
	subu	$sp, $sp, $t0
	ld	$t6, 0($sp)
	subu	$sp, $sp, $t0
	ld	$t5, 0($sp)
	subu	$sp, $sp, $t0
	ld	$t4, 0($sp)
	subu	$sp, $sp, $t0
	ld	$t3, 0($sp)
	subu	$sp, $sp, $t0
	ld	$t2, 0($sp)
	subu	$sp, $sp, $t0
	ld	$t1, 0($sp)
	subu	$sp, $sp, $t0
	jr	$ra
	ld	$t0, 0($sp)

do_xsum:
	jal	make_xsum
	nop
	eret

generate_random_u8:
	lbu	$a0, HW_REG_RANDOM($zero)
	eret

generate_random_u16:
	lhu	$a0, HW_REG_RANDOM($zero)
	eret

generate_random_u32:
	lwu	$a0, HW_REG_RANDOM($zero)
	eret

generate_random_s8:
	lb	$a0, HW_REG_RANDOM($zero)
	eret

generate_random_s16:
	lh	$a0, HW_REG_RANDOM($zero)
	eret

generate_random_s32:
	lw	$a0, HW_REG_RANDOM($zero)
	eret

generate_random:
	ld	$a0, HW_REG_RANDOM($zero)
	eret

syscall_list:				/* Switch case */
.word	0x00000000
.word	print_hex			/* Case 1 */
.word	print_text			/* Case 2 */
.word	print_int			/* Case 3 */
.word	get_hardware_version		/* Case 4 */
.word	get_bootstrap_version		/* Case 5 */
.word	get_keys_bits			/* Case 6 */
.word	get_key_down			/* Case 7 */
.word	get_key_up			/* Case 8 */
.word	do_xsum				/* Case 9 */
.word	generate_random_u8		/* Case 10 */
.word	generate_random_u16		/* Case 11 */
.word	generate_random_u32		/* Case 12 */
.word	generate_random_s8		/* Case 13 */
.word	generate_random_s16		/* Case 14 */
.word	generate_random_s32		/* Case 15 */
.word	generate_random			/* Case 16 */

syscall_handler:
	li	$k1, 4
	multu	$v0, $k1
	mfhi	$k0
	lw	$k0, syscall_list($k0)
	beqz	$k0, quit_syscall
	nop
	jr	$k0
	nop
quit_syscall:
	eret


	.org 0x1000
init_mach:						/* Inits the machine and does BIOS testing */
	move	$s7, $ra				/* Save the link register */
	ori	$a0, $zero, HW_POWERON_GRAPHICS		/* Load up graphics. */
	sw	$a0, HW_REG_POWER($zero)		/* If this fails, the user's sorta fucked... */
	
	la	$a0, about_1
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	li	$a0, BOOTSTRAP_MAJOR
	sd	$a0, HW_REG_PRINT_INT($zero)
	la	$a0, period
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	li	$a0, BOOTSTRAP_MINOR
	sd	$a0, HW_REG_PRINT_INT($zero)
	la	$a0, period
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	li	$a0, BOOTSTRAP_REV
	sd	$a0, HW_REG_PRINT_INT($zero)
	la	$a0, about_2
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	la	$a0, about_3
	sw	$a0, HW_REG_PRINT_TEXT($zero)

	la	$a0, testing_1
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	la	$a0, testing_2
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	lw	$a0, HW_REG_POWER($zero)
	andi	$a0, $a0, HW_POWERON_GRAPHICS
	beqz	$a0, failed
	nop
	la	$a0, success
	sw	$a0, HW_REG_PRINT_TEXT($zero)

	la	$a0, testing_3
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	lw	$a0, HW_REG_POWER($zero)
	ori	$a0, $a0, HW_POWERON_INPUT
	sw	$a0, HW_REG_POWER($zero)
	lw	$a0, HW_REG_POWER($zero)
	andi	$a0, $a0, HW_POWERON_INPUT
	beqz	$a0, failed
	nop
	la	$a0, success
	sw	$a0, HW_REG_PRINT_TEXT($zero)

	li	$a0, CHECK_MEMORY
	beqz	$a0, skip_memcheck
	nop

	la	$a0, testing_4
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	
	li	$a0, 0x10000000
	li	$a1, 0x00000100
	jal	verify_chunk
	li	$a2, 1

	li	$a0, 0x20000000
	li	$a1, 0x00010000
	jal	verify_chunk
	li	$a2, 0
	
	li	$a0, 0x80000000
	li	$a1, 0x00400000
	jal	verify_chunk
	li	$a2, 0
	
	li	$a0, 0xBFC00000
	li	$a1, 0x00400000
	jal	verify_chunk
	li	$a2, 1
	
	li	$a0, 0xC0000000
	li	$a1, 0x08000000
	jal	verify_chunk
	li	$a2, 1

skip_memcheck:
	la	$a0, testing_5
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	li	$a0, CHECK_XSUM
	beqz	$a0, skip_xsum_check
	nop

	la	$a0, 0xBFC00000
	move	$a1, $zero
	jal	make_xsum
	lui	$a1, 0x0030
	ld	$a1, valid_xsum
	beqz	$a1, skip_xsum_check
	nop
	bne	$a0, $a1, failed
	nop
skip_xsum_check:
	la	$k0, success
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	
	la	$a0, testing_6
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	sb	$zero, HW_REG_SHUFFLE($zero)
	la	$k0, success
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	
	la	$a0, testing_done
	sw	$a0, HW_REG_PRINT_TEXT($zero)
	jr	$s7
	nop

verify_chunk:
	la	$k0, testing_memory
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	sw	$a0, HW_REG_PRINT_HEX($zero)
	la	$k0, dash
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	addu	$a1, $a0, $a1
	sw	$a1, HW_REG_PRINT_HEX($zero)
	la	$k0, space
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	li	$k0, 0xDEADB007
	dsll	$k0, $k0, 32
	li	$k1, 0xC0DEBABE
	daddu	$k0, $k0, $k1
	move	$k1, $k0
	beqz	$a2, verify_ram_chunk
	nop
verify_rom_loop:
	ld      $k0, 0($a0)
	addiu	$a0, $a0, 8
	bne	$a0, $a1, verify_rom_loop
	nop
	la	$k0, success
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	jr	$ra
	nop
verify_ram_chunk:
	sd      $k1, 0($a0)
	bne	$k0, $k1, failed
	ld      $k0, 0($a0)
	addiu	$a0, $a0, 8
	bne	$a0, $a1, verify_ram_chunk
	nop
	la	$k0, success
	sw	$k0, HW_REG_PRINT_TEXT($zero)
	jr	$ra
	nop

