/*****************************************************************************
 *  µChecksum 0.2                                                            *
 *  Just runs the checksum from the Bootstrap. Originally from here though!  *
 *  (C) 2009 - The Lemon Man                                                 *
 *  (C) 2009 - SquidMan                                                      *
 *****************************************************************************/
	.set	noreorder
	.set	nomove

	.include "syscall.inc"

.section .info, "wa", @progbits
info:
	.asciiz "µChecksum 0.2 - (C) 2009 - The Lemon Man & SquidMan\nRunning bootstrap v"
info2:
	.asciiz "\nHardware version "
xsumstr:
	.asciiz "Checksum: "
newline:
	.asciiz "\n"
bootstrap_dot:
	.asciiz "."

.section .code, "ax", @progbits
_main:
	move	$t6, $a0
	la	$a0, info
	syscall_print_text

	move	$a0, $t6
	syscall_print_int

	la	$a0, bootstrap_dot
	syscall_print_text

	move	$a0, $a1
	syscall_print_int

	la	$a0, bootstrap_dot
	syscall_print_text

	move	$a0, $a2
	syscall_print_int

	la	$a0, info2
	syscall_print_text

	move	$a0, $a3
	syscall_print_int

	la	$a0, newline
	syscall_print_text

	/* Point the streameater to 0xBFC00000 and 0x00300000 as size. */
	li	$a0, 0xBFC00000
	li	$a1, 0x00300000
	syscall_make_checksum
	move	$s1, $a0

	la	$a0, xsumstr
	syscall_print_text

	move	$a0, $s1
	syscall_print_hex

	la	$a0, newline
	syscall_print_text
	syscall_random
	syscall_print_hex
	la	$a0, newline
	syscall_print_text
hang:
	j	hang
	nop

_exception:			/* Ignore any exceptions */
	jr	$s7
	nop

.section .bootsupport, "a", @progbits
	.word	_main
	.word	_exception
