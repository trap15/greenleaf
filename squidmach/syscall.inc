/*****************************************************************************
 *  SquidMach Bootstrap Syscall list                                         *
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *****************************************************************************/

	.equ	SYSCALLS_PRINT_HEX,			1
	.equ	SYSCALLS_PRINT_TEXT,			2
	.equ	SYSCALLS_PRINT_INT,			3
	.equ	SYSCALLS_GET_HW_VERSION,		4
	.equ	SYSCALLS_GET_BOOTSTRAP_VERSION,		5
	.equ	SYSCALLS_GET_KEYS_BITS,			6
	.equ	SYSCALLS_GET_KEY_DOWN,			7
	.equ	SYSCALLS_GET_KEY_UP,			8
	.equ	SYSCALLS_MAKE_CHECKSUM,			9
	.equ	SYSCALLS_RANDOM_U8,			10
	.equ	SYSCALLS_RANDOM_U16,			11
	.equ	SYSCALLS_RANDOM_U32,			12
	.equ	SYSCALLS_RANDOM_S8,			13
	.equ	SYSCALLS_RANDOM_S16,			14
	.equ	SYSCALLS_RANDOM_S32,			15
	.equ	SYSCALLS_RANDOM,			16

.macro	save_v0
	sd	$v0, 0($sp)
	addiu	$sp, $sp, 8
.endm

.macro	restore_v0
	li	$k0, 8
	subu	$sp, $sp, $k0
	ld	$v0, 0($sp)
.endm

.macro	syscall_print_text
	save_v0
	li	$v0, SYSCALLS_PRINT_TEXT
	syscall
	restore_v0
.endm

.macro	syscall_print_int
	save_v0
	li	$v0, SYSCALLS_PRINT_INT
	syscall
	restore_v0
.endm

.macro	syscall_print_hex
	save_v0
	li	$v0, SYSCALLS_PRINT_HEX
	syscall
	restore_v0
.endm

.macro	syscall_get_hw_version
	save_v0
	li	$v0, SYSCALLS_GET_HW_VERSION
	syscall
	restore_v0
.endm

.macro	syscall_get_bootstrap_version
	save_v0
	li	$v0, SYSCALLS_GET_BOOTSTRAP_VERSION
	syscall
	restore_v0
.endm

.macro	syscall_get_keys_bits
	save_v0
	li	$v0, SYSCALLS_GET_KEYS_BITS
	syscall
	restore_v0
.endm

.macro	syscall_get_key_down
	save_v0
	li	$v0, SYSCALLS_GET_KEY_DOWN
	syscall
	restore_v0
.endm

.macro	syscall_get_key_up
	save_v0
	li	$v0, SYSCALLS_GET_KEY_UP
	syscall
	restore_v0
.endm

.macro	syscall_make_checksum
	save_v0
	li	$v0, SYSCALLS_MAKE_CHECKSUM
	syscall
	restore_v0
.endm

.macro	syscall_random_u8
	save_v0
	li	$v0, SYSCALLS_RANDOM_U8
	syscall
	restore_v0
.endm

.macro	syscall_random_u16
	save_v0
	li	$v0, SYSCALLS_RANDOM_U16
	syscall
	restore_v0
.endm

.macro	syscall_random_u32
	save_v0
	li	$v0, SYSCALLS_RANDOM_U32
	syscall
	restore_v0
.endm

.macro	syscall_random_s8
	save_v0
	li	$v0, SYSCALLS_RANDOM_S8
	syscall
	restore_v0
.endm

.macro	syscall_random_s16
	save_v0
	li	$v0, SYSCALLS_RANDOM_S16
	syscall
	restore_v0
.endm

.macro	syscall_random_s32
	save_v0
	li	$v0, SYSCALLS_RANDOM_S32
	syscall
	restore_v0
.endm

.macro	syscall_random
	save_v0
	li	$v0, SYSCALLS_RANDOM
	syscall
	restore_v0
.endm
