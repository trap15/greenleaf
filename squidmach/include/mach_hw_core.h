#ifndef MACH_HW_REGS_H
#define MACH_HW_REGS_H

#include "types.h"
#include "emulator.h"
#include "graphics.h"
#include "input.h"

u8   readHWRegByte  (mipsCpu* cpu, u32 address);
u16  readHWRegHword (mipsCpu* cpu, u32 address);
u32  readHWRegWord  (mipsCpu* cpu, u32 address);
u64  readHWRegDword (mipsCpu* cpu, u32 address);
void writeHWRegByte (mipsCpu* cpu, u32 address, u8  value);
void writeHWRegHword(mipsCpu* cpu, u32 address, u16 value);
void writeHWRegWord (mipsCpu* cpu, u32 address, u32 value);
void writeHWRegDword(mipsCpu* cpu, u32 address, u64 value);
int  HWInit         ();

INLINE void HWPreUpdate()
{
	input_pre_update();
	graphics_pre_update();
}

INLINE void HWPostUpdate()
{
	graphics_post_update();
	input_post_update();
}


#endif
