#ifndef MACH_MEMORY_H
#define MACH_MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include "memory.h"
#include "types.h"
#include "emulator.h"

#include "mach_hw_core.h"

INLINE int memWithinRange(u32 addr1, u32 size1, u32 addr2, u32 size2)
{
	if(addr1 <= addr2)
		if((addr1 + size1) > (addr2 + size2))
			return 1;
	return 0;
}

INLINE u32 transformAddr(mipsCpu* cpu, u32 address, int size, mipsMappedMemory **bank, int type)
{
	*bank = getBank(cpu, address, size, type);
	if(*bank == NULL) {
		printf("EXCEPTION : %s(%#x)\n", __FUNCTION__, address);
		if(type == 0)
			type = 2;
		generateException(cpu, EXCEPTION_BADADDR_LOAD + type - 1, 0);
		exit(1);
	}
	int addr = (address - (*bank)->addrStart);
	return addr;
}

INLINE u8 readByte(mipsCpu* cpu, u32 address)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 1))
		return readHWRegByte(cpu, address);
	address = transformAddr(cpu, address, 1, &bank, 1);
	return (bank->memory[address]);
}

INLINE u16 readHword(mipsCpu* cpu, u32 address)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 2))
		return readHWRegHword(cpu, address);
	address = transformAddr(cpu, address, 2, &bank, 1);
#if _LE_
	return *((u16*)(&(bank->memory[address])));
#else
	return	(bank->memory[address + 1] <<  8) |
		(bank->memory[address + 0] <<  0) ;
#endif
}

INLINE u32 readWord(mipsCpu* cpu, u32 address)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 4))
		return readHWRegWord(cpu, address);
	address = transformAddr(cpu, address, 4, &bank, 1);
#if _LE_
	return *((u32*)(&(bank->memory[address])));
#else
	return	(bank->memory[address + 3] << 24) |
		(bank->memory[address + 2] << 16) |
		(bank->memory[address + 1] <<  8) |
		(bank->memory[address + 0] <<  0) ;
#endif
}

INLINE u64 readDword(mipsCpu* cpu, u32 address)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 8))
		return readHWRegDword(cpu, address);
	address = transformAddr(cpu, address, 8, &bank, 1);
#if _LE_
	return *((u64*)(&(bank->memory[address])));
#else
	return	((u64)bank->memory[address + 7] << 56) |
		((u64)bank->memory[address + 6] << 48) |
		((u64)bank->memory[address + 5] << 40) |
		((u64)bank->memory[address + 4] << 32) |
		((u64)bank->memory[address + 3] << 24) |
		((u64)bank->memory[address + 2] << 16) |
		((u64)bank->memory[address + 1] <<  8) |
		((u64)bank->memory[address + 0] <<  0) ;
#endif
}

INLINE u32 readOpcode(mipsCpu* cpu, u32 address)
{
	mipsMappedMemory* bank;
	address = transformAddr(cpu, address, 4, &bank, 1);
#if _LE_
	return *((u32*)(&(bank->memory[address])));
#else
	return	(bank->memory[address + 3] << 24) |
		(bank->memory[address + 2] << 16) |
		(bank->memory[address + 1] <<  8) |
		(bank->memory[address + 0] <<  0) ;
#endif
}

INLINE void writeByte(mipsCpu* cpu, u32 address, u8 value)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 1)) {
		writeHWRegByte(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 1, &bank, 2);
	bank->memory[address] = value;
}

INLINE void writeHword(mipsCpu* cpu, u32 address, u16 value)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 2)) {
		writeHWRegHword(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 2, &bank, 2);
#if _LE_
	*((u16*)(&(bank->memory[address]))) = value;
#else
	bank->memory[address + 1] = (u8)(value >>  8);
	bank->memory[address + 0] = (u8)(value >>  0);
#endif
}

INLINE void writeWord(mipsCpu* cpu, u32 address, u32 value)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 4)) {
		writeHWRegWord(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 4, &bank, 2);
#if _LE_
	*((u32*)(&(bank->memory[address]))) = value;
#else
	bank->memory[address + 3] = (u8)(value >> 24);
	bank->memory[address + 2] = (u8)(value >> 16);
	bank->memory[address + 1] = (u8)(value >>  8);
	bank->memory[address + 0] = (u8)(value >>  0);
#endif
}

INLINE void writeDword(mipsCpu* cpu, u32 address, u64 value)
{
	mipsMappedMemory* bank;
	if(memWithinRange(HW_REGS_ADDR, HW_REGS_SIZE, address, 8)) {
		writeHWRegDword(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 8, &bank, 2);
#if _LE_
	*((u64*)(&(bank->memory[address]))) = value;
#else
	bank->memory[address + 7] = (u8)(value >> 56);
	bank->memory[address + 6] = (u8)(value >> 48);
	bank->memory[address + 5] = (u8)(value >> 40);
	bank->memory[address + 4] = (u8)(value >> 32);
	bank->memory[address + 3] = (u8)(value >> 24);
	bank->memory[address + 2] = (u8)(value >> 16);
	bank->memory[address + 1] = (u8)(value >>  8);
	bank->memory[address + 0] = (u8)(value >>  0);
#endif
}

/*
 * Memory functions
 */

INLINE void memcopy(mipsCpu* cpu, void *src, u32 address, int size)
{
	u32 x;
	u8* s = src;
	mipsMappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 2);
	for (x = 0; x < size; x++, s++)
		bank->memory[address + x] = *s;
}

INLINE void memcopy_load(mipsCpu* cpu, void *src, u32 address, int size)
{
	u32 x;
	u8* s = src;
	mipsMappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 0);
	for (x = 0; x < size; x++, s++)
		bank->memory[address + x] = *s;
}

INLINE void memoryset(mipsCpu* cpu, u32 address, u8 fill, int size)
{
	int x;
	mipsMappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 2);
	for (x = 0; x < size; x++)
		bank->memory[address + x] = fill;
}

INLINE void memoryset_load(mipsCpu* cpu, u32 address, u8 fill, int size)
{
	int x;
	mipsMappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 0);
	for (x = 0; x < size; x++)
		bank->memory[address + x] = fill;
}



#endif
