#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "types.h"
#include "memory.h"

#include "mach_memory.h"
#include "graphics.h"
#include "input.h"

extern u32 hardware_power;

INLINE u32 getHWRevision(mipsCpu* cpu)
{
	return HW_VERSION;
}

INLINE u32 doHWPowerRead(mipsCpu* cpu)
{
	return hardware_power;
}

INLINE void doHWPowerWrite(mipsCpu* cpu, u32 value)
{
	if(value & HW_POWERON_GRAPHICS) {
#ifdef DEBUG
		printf("Graphics turned on.\n");
#endif
		if(!graphics_poweron()) {
			hardware_power &= ~HW_POWERON_GRAPHICS;
#ifdef DEBUG
			printf("FAILED.\n");
#endif
		}else{
			hardware_power |= HW_POWERON_GRAPHICS;
#ifdef DEBUG
			printf("SUCCESS.\n");
#endif
		}
	}
	if(value & HW_POWERON_INPUT) {
#ifdef DEBUG
		printf("Input turned on.\n");
#endif
		if(!input_poweron()) {
			hardware_power &= ~HW_POWERON_INPUT;
#ifdef DEBUG
			printf("FAILED.\n");
#endif
		}else{
			hardware_power |= HW_POWERON_INPUT;
#ifdef DEBUG
			printf("SUCCESS.\n");
#endif
		}
	}
/*	if(value & HW_POWERON_SOUND) {
#ifdef DEBUG
		printf("Sound turned on.\n");
#endif
		if(!sound_poweron()) {
			hardware_power &= ~HW_POWERON_SOUND;
#ifdef DEBUG
			printf("FAILED.\n");
#endif
		}else{
			hardware_power |= HW_POWERON_SOUND;
#ifdef DEBUG
			printf("SUCCESS.\n");
#endif
		}
	}*/
}

INLINE void doHWPrintText(mipsCpu* cpu, u32 addr)
{
	char s[256];
	int i;
#ifdef DEBUG
	printf("Printing Text: ");
#endif
	for(i = 0; ((s[i] = readByte(cpu, addr + i)) != 0) && (i < 256); i++);
	graphics_printstr(s);
}

INLINE void doHWPrintHex64(mipsCpu* cpu, u64 val)
{
#ifdef DEBUG
	printf("Printing Hex: ");
#endif
	graphics_printhex(val, 8);
}

INLINE void doHWPrintInt64(mipsCpu* cpu, s64 val)
{
#ifdef DEBUG
	printf("Printing Integer: ");
#endif
	graphics_printint(val);
}

INLINE void doHWPrintHex32(mipsCpu* cpu, u32 val)
{
#ifdef DEBUG
	printf("Printing Hex: ");
#endif
	graphics_printhex(val, 4);
}

INLINE void doHWPrintInt32(mipsCpu* cpu, s32 val)
{
#ifdef DEBUG
	printf("Printing Integer: ");
#endif
	graphics_printint(val);
}

INLINE void doHWPrintHex16(mipsCpu* cpu, u16 val)
{
#ifdef DEBUG
	printf("Printing Hex: ");
#endif
	graphics_printhex(val, 2);
}

INLINE void doHWPrintInt16(mipsCpu* cpu, s16 val)
{
#ifdef DEBUG
	printf("Printing Integer: ");
#endif
	graphics_printint(val);
}

INLINE void doHWPrintHex8(mipsCpu* cpu, u8 val)
{
#ifdef DEBUG
	printf("Printing Hex: ");
#endif
	graphics_printhex(val, 1);
}

INLINE void doHWPrintInt8(mipsCpu* cpu, s8 val)
{
#ifdef DEBUG
	printf("Printing Integer: ");
#endif
	graphics_printint(val);
}

INLINE u64 doHWReadKeys(mipsCpu* cpu, int c)
{
	return input_getkeys_bitfield(c);
}

INLINE u64 randCore()
{
	u64 v = rand();
	v *= rand() + 1;
	v *= rand() + 1;
	v *= rand() + 1;
	return v;
}

INLINE u8 doHWRandom8(mipsCpu* cpu)
{
	return randCore() % 0xFF;
}

INLINE u16 doHWRandom16(mipsCpu* cpu)
{
	return randCore() % 0xFFFF;
}

INLINE u32 doHWRandom32(mipsCpu* cpu)
{
	return randCore() % 0xFFFFFFFF;
}

INLINE u64 doHWRandom64(mipsCpu* cpu)
{
	return randCore();
}

INLINE void doHWRandomShuffle(mipsCpu* cpu)
{
	int seed;
	int seedgarbage1, seedgarbage2, seedgarbage3, seedgarbage4;
	seed = time(NULL) * (seedgarbage1 + 1);
	seed += seedgarbage2;
	srand(seed);
	seed -= (seedgarbage3 % (rand() + 1));
	seed *= time(NULL);
	seed *= seedgarbage4 + 1;
	srand(seed);
}	
