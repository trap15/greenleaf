#ifndef INPUT_H_
#define INPUT_H_

#include "types.h"

#include "SDL.h"
#include <stdio.h>

extern u64 input_keys_down[2];
extern int input_enabled;

#define BUTTON_MAP(x, y)					\
	case SDLK_##x:						\
		f = y - 1;					\
		if(y <= 64)					\
			z = &input_keys_down[0];		\
		else{						\
			z = &input_keys_down[1];		\
			f -= 64;				\
		}						\
		if(t == 1)					\
			*z &= ~(1LL << f);			\
		else						\
			*z |= 1LL << f;				\
		break;

INLINE int input_setup()
{
	input_enabled = 0;
	input_keys_down[0] = 0;
	input_keys_down[1] = 0;
	return 1;
}

INLINE void input_handle_keycore(SDL_Event e, int t)
{
	u64* z = NULL;
	u64 f;
	switch(e.key.keysym.sym) {
		case SDLK_F12:
			printf("Quitting...\n");
			exit(0);
			break;
		BUTTON_MAP(ESCAPE,		2)
		BUTTON_MAP(1,			3)
		BUTTON_MAP(2,			4)
		BUTTON_MAP(3,			5)
		BUTTON_MAP(4,			6)
		BUTTON_MAP(5,			7)
		BUTTON_MAP(6,			8)
		BUTTON_MAP(7,			9)
		BUTTON_MAP(8,			10)
		BUTTON_MAP(9,			11)
		BUTTON_MAP(0,			12)
		BUTTON_MAP(MINUS,		13)
//		BUTTON_MAP(NONE,		14)
//		BUTTON_MAP(NONE,		15)
		BUTTON_MAP(BACKSPACE,		16)
		BUTTON_MAP(TAB,			17)
		BUTTON_MAP(q,			18)
		BUTTON_MAP(w,			19)
		BUTTON_MAP(e,			20)
		BUTTON_MAP(r,			21)
		BUTTON_MAP(t,			22)
		BUTTON_MAP(y,			23)
		BUTTON_MAP(u,			24)
		BUTTON_MAP(i,			25)
		BUTTON_MAP(o,			26)
		BUTTON_MAP(p,			27)
		BUTTON_MAP(LEFTBRACKET,		28)
		BUTTON_MAP(RIGHTBRACKET,	29)
		BUTTON_MAP(RETURN,		30)
		BUTTON_MAP(a,			31)
		BUTTON_MAP(s,			32)
		BUTTON_MAP(d,			33)
		BUTTON_MAP(f,			34)
		BUTTON_MAP(g,			35)
		BUTTON_MAP(h,			36)
		BUTTON_MAP(j,			37)
		BUTTON_MAP(k,			38)
		BUTTON_MAP(l,			39)
		BUTTON_MAP(COLON,		40)
//		BUTTON_MAP(NONE,		41)
		BUTTON_MAP(z,			42)
		BUTTON_MAP(x,			43)
		BUTTON_MAP(c,			44)
		BUTTON_MAP(v,			45)
		BUTTON_MAP(b,			46)
		BUTTON_MAP(n,			47)
		BUTTON_MAP(m,			48)
		BUTTON_MAP(COMMA,		49)
		BUTTON_MAP(PERIOD,		50)
		BUTTON_MAP(SLASH,		51)
//		BUTTON_MAP(NONE,		52)
		BUTTON_MAP(SPACE,		53)
		BUTTON_MAP(HOME,		54)
		BUTTON_MAP(DELETE,		55)
		BUTTON_MAP(PAGEUP,		56)
		BUTTON_MAP(PAGEDOWN,		57)
//		BUTTON_MAP(NONE,		58)
		BUTTON_MAP(LEFT,		59)
		BUTTON_MAP(UP,			60)
		BUTTON_MAP(RIGHT,		61)
		BUTTON_MAP(DOWN,		62)
//		BUTTON_MAP(CLEAR,		63)
		BUTTON_MAP(KP_DIVIDE,		64)
		BUTTON_MAP(KP_MULTIPLY,		65)
		BUTTON_MAP(KP_MINUS,		66)
		BUTTON_MAP(KP7,			67)
		BUTTON_MAP(KP8,			68)
		BUTTON_MAP(KP9,			69)
		BUTTON_MAP(KP_PLUS,		70)
		BUTTON_MAP(KP4,			71)
		BUTTON_MAP(KP5,			72)
		BUTTON_MAP(KP6,			73)
		BUTTON_MAP(KP_EQUALS,		74)
		BUTTON_MAP(KP1,			75)
		BUTTON_MAP(KP2,			76)
		BUTTON_MAP(KP3,			77)
		BUTTON_MAP(KP_ENTER,		78)
		BUTTON_MAP(KP0,			79)
//		BUTTON_MAP(NONE,		80)
		BUTTON_MAP(KP_PERIOD,		81)
//		BUTTON_MAP(NONE,		82)
//		BUTTON_MAP(NONE,		83)
//		BUTTON_MAP(NONE,		84)
//		BUTTON_MAP(NONE,		85)
//		BUTTON_MAP(NONE,		86)
//		BUTTON_MAP(NONE,		87)
//		BUTTON_MAP(NONE,		88)
//		BUTTON_MAP(NONE,		89)
//		BUTTON_MAP(NONE,		90)
//		BUTTON_MAP(NONE,		91)
//		BUTTON_MAP(NONE,		92)
		BUTTON_MAP(CAPSLOCK,		93)
		BUTTON_MAP(INSERT,		94)
//		BUTTON_MAP(NONE,		95)
//		BUTTON_MAP(NONE,		96)
//		BUTTON_MAP(NONE,		97)
//		BUTTON_MAP(NONE,		98)
		BUTTON_MAP(F1,			99)
		BUTTON_MAP(F2,			100)
		BUTTON_MAP(F3,			101)
		BUTTON_MAP(F4,			102)
		BUTTON_MAP(F5,			103)
		BUTTON_MAP(F6,			104)
		BUTTON_MAP(F7,			105)
		BUTTON_MAP(F8,			106)
		BUTTON_MAP(F9,			107)
		BUTTON_MAP(F10,			108)
//		BUTTON_MAP(NONE,		109)
//		BUTTON_MAP(NONE,		110)
//		BUTTON_MAP(NONE,		111)
		BUTTON_MAP(LSHIFT,		112)
		BUTTON_MAP(LCTRL,		113)
		BUTTON_MAP(RSHIFT,		112)
		BUTTON_MAP(RCTRL,		113)
		default:
			printf("Dunno...\n");
			break;
	}
}

INLINE void input_handle_keydown(SDL_Event e)
{
	input_handle_keycore(e, 0);
}

INLINE void input_handle_keyup(SDL_Event e)
{
	input_handle_keycore(e, 1);
}

INLINE void input_pre_update()
{
	SDL_Event e;
	while(SDL_PollEvent(&e)) {
		switch(e.type) {
			case SDL_KEYDOWN:
				input_handle_keydown(e);
				break;
			case SDL_KEYUP:
				input_handle_keyup(e);
				break;
			case SDL_QUIT:
				printf("Quitting...\n");
				exit(0);
				break;
			default:
				break;
		}
	}
}

INLINE void input_post_update()
{
	
}

INLINE int input_poweron()
{
	if(input_enabled) {
		input_enabled = 0;
		input_keys_down[0] &= ~(1LL);
		return 1;
	}else{
		input_enabled = 1;
		input_keys_down[0] |=   1LL;
		return 1;
	}
}

INLINE u64 input_getkeys_bitfield(int i)
{
	if(!input_enabled)
		return 0;
	if((i != 0) && (i != 1))
		return 0;
	return input_keys_down[i];
}

#endif /* INPUT_H_ */
