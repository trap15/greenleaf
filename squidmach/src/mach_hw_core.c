#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "binary.h"
#include "types.h"
#include "emulator.h"
#include "memory.h"

#include "mach_memory.h"
#include "mach_hw_core.h"
#include "mach_hw.h"
#include "graphics.h"
#include "input.h"

u32 hardware_power = 0;

u8 readHWRegByte(mipsCpu* cpu, u32 address)
{
	u8 ret = 0;
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_RANDOM:
			ret = doHWRandom8(cpu);
			break;
		default:
			break;
	}
	return ret;
}

u16 readHWRegHword(mipsCpu* cpu, u32 address)
{
	u16 ret = 0;
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_RANDOM:
			ret = doHWRandom16(cpu);
			break;
		default:
			break;
	}
	return ret;
}

u32 readHWRegWord(mipsCpu* cpu, u32 address)
{
	u32 ret = 0;
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_RANDOM:
			ret = doHWRandom32(cpu);
			break;
		case HW_ADDRESS_VERSION:
			ret = getHWRevision(cpu);
			break;
		case HW_ADDRESS_POWER:
			ret = doHWPowerRead(cpu);
			break;
		default:
			break;
	}
	return ret;
}

u64 readHWRegDword(mipsCpu* cpu, u32 address)
{
	u64 ret = 0LL;
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_RANDOM:
			ret = doHWRandom64(cpu);
			break;
		case HW_ADDRESS_READINPUT_1:
			ret = doHWReadKeys(cpu, 0);
			break;
		case HW_ADDRESS_READINPUT_2:
			ret = doHWReadKeys(cpu, 1);
			break;
		default:
			break;
	}
	return ret;
}

void writeHWRegByte(mipsCpu* cpu, u32 address, u8 value)
{
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_SHUFFLE:
			doHWRandomShuffle(cpu);
			break;
		case HW_ADDRESS_PRINT_HEX:
			doHWPrintHex8(cpu, value);
			break;
		case HW_ADDRESS_PRINT_INT:
			doHWPrintInt8(cpu, value);
			break;
		default:
			break;
	}
}

void writeHWRegHword(mipsCpu* cpu, u32 address, u16 value)
{
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_PRINT_HEX:
			doHWPrintHex16(cpu, value);
			break;
		case HW_ADDRESS_PRINT_INT:
			doHWPrintInt16(cpu, value);
			break;
		default:
			break;
	}
}

void writeHWRegWord(mipsCpu* cpu, u32 address, u32 value)
{
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_PRINT_TEXT:
			doHWPrintText(cpu, value);
			break;
		case HW_ADDRESS_PRINT_HEX:
			doHWPrintHex32(cpu, value);
			break;
		case HW_ADDRESS_PRINT_INT:
			doHWPrintInt32(cpu, value);
			break;
		case HW_ADDRESS_POWER:
			doHWPowerWrite(cpu, value);
			break;
		default:
			break;
	}
}

void writeHWRegDword(mipsCpu* cpu, u32 address, u64 value)
{
	address -= HW_REGS_ADDR;
	switch(address) {
		case HW_ADDRESS_PRINT_HEX:
			doHWPrintHex64(cpu, value);
			break;
		case HW_ADDRESS_PRINT_INT:
			doHWPrintInt64(cpu, value);
			break;
		default:
			break;
	}
}

int HWInit()
{
	if(!graphics_setup())
		return 0;
	if(!input_setup())
		return 0;
	return 1;
}

