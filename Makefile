all:
	make -C squidmach all
clean:
	make -C squidmach clean
run:
	make -C squidmach run
bootstrap:
	make -C squidmach bootstrap
cleanbootstrap:
	make -C squidmach cleanbootstrap
testrom:
	make -C squidmach testrom
cleantestrom:
	make -C squidmach cleantestrom
release:
	make -C squidmach release





